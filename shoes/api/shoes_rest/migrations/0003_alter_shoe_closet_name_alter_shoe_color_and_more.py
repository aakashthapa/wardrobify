# Generated by Django 4.0.3 on 2024-01-31 19:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_shoe_color_shoe_manufacturer_shoe_model_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoe',
            name='closet_name',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='shoe',
            name='color',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='shoe',
            name='manufacturer',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='shoe',
            name='model',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
