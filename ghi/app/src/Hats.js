import React, { useEffect, useState } from 'react';
function HatList() {
    const [hats, setHats] = useState([]);
    const getData = async () => {
        const response = await fetch('http://localhost:8091/api/hats/');
        if (response.ok) {
            const { hats } = await response.json();
            setHats(hats);
        } else {
            console.error('An error occurred fetching the data');
        }
    };
    async function handleDelete(id) {
        try {
          const response = await fetch(`http://localhost:8091/api/hats/${id}/`, {
            method: "DELETE"
          });
          if (response.ok) {
            getData();  // Refresh the hat list after deletion
          } else {
          }
        } catch (error) {
          console.error('An unexpected error occurred:', error);
        }
      }
    useEffect(() => {
        getData();
    }, []);
    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Hat List</h1>
                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Hat style</th>
                            <th>Color</th>
                            <th>Fabric</th>
                            <th>Picture</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {hats.map(hat => (
                            <tr key={hat.id}>
                                <td>{hat.location}</td>
                                <td>{hat.style}</td>
                                <td>{hat.color}</td>
                                <td>{hat.fabric}</td>
                                <td>
                                    <img
                                        src={hat.picture_url}
                                        alt=""
                                        width="100px"
                                        height="100px"
                                    />
                                </td>
                                <td>
                                    <button
                                        onClick={() => handleDelete(hat.id)}
                                        className="btn btn-danger"
                                    >
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
export default HatList;





