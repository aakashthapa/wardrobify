import json
from django.shortcuts import render
from django.http import JsonResponse
from .models import Shoe
from common.json import ModelEncoder
from .models import BinVO
from django.views.decorators.http import require_http_methods

######################################
##Encoders##

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href"]

class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
    "closet_name",
    "shoe_size",
    "manufacturer",
    "model",
    "color",
    "picture_url",
    "bin"
    ]
    encoders = {
     "bin": BinVOEncoder(),
    }

######################################
##Views##
@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe(request, pk):
    print(pk)
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)

            props = [
    "closet_name",
    "shoe_size",
    "manufacturer",
    "model",
    "color",
    "picture_url",
    ]

            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )

        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_shoes_list(request):
    if request.method == "GET":
            shoes = Shoe.objects.all()
            data = {"shoes": list(shoes.values())}
            return JsonResponse(
            shoes,
            encoder=ShoeEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin

            shoe = Shoe.objects.create(**content)
            return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400
        )

######################################
