from django.urls import path
from .views import api_shoe, api_shoes_list

urlpatterns = [
    path('shoes/', api_shoes_list, name='api_shoes_list'),
    path('shoes/<int:pk>/', api_shoe, name='api_shoe'),
]
