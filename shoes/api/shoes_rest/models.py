from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=200)
# Create your models here.




class Shoe(models.Model):
    closet_name = models.CharField(max_length=100, null=True)
    shoe_size = models.PositiveSmallIntegerField()
    manufacturer = models.CharField(max_length=100, null=True)
    model = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=100, null=True)
    picture_url = models.URLField(null=True)
    


    def get_api_url(self):
        return reverse("api_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.closet_name} - {self.shoe_size}"

    class Meta:
        ordering = ("closet_name", "shoe_size")

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null = True
    )

#makeforeignkey
