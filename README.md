# Wardrobify

Team:

* Aakash Thapa - Hats
* Wesley Sparzak - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

In the Hats microservice, we designed a model called Hat to represent unique entities within our microservice's database. Each hat can be identified by a unique ID within our system. Additionally, we included a foreign key for the location field in the Hat model, allowing each hat to be associated with a specific location. Since multiple hats can be located in the same place, the location field references the Location Value Object (VO) model.

The Location VO model is a value object that represents location data. It is designed to be immutable and fungible, meaning it does not change and can be freely exchanged or replaced. This model is used for reference data and does not actively participate in the modification of data within our system. Instead, it serves as a stable reference for the locations associated with hats.

To integrate with the wardrobe microservice, we implemented a poller within our Hats microservice. This poller periodically requests data from the wardrobe API port to retrieve the latest location information. When new location data is obtained, we update our own Location VO database to reflect the changes. This approach allows us to maintain a synchronized copy of the location data from the wardrobe microservice without directly modifying its data.