import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from shoes_rest, here.
# from shoes_rest.models import Something

import requests
from .models import BinVO

def get_bin():
    url = "http://wardrobe-api:8000/api/bins"
    response = requests.get(url)
    content = response.json()
    print ("Just polled...")

    for bin in content["bin"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"], #unique identifier
            defaults={"name": bin["name"], "description": bin["description"]} #everything else
        )

def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
