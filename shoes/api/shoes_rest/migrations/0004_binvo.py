# Generated by Django 4.0.3 on 2024-01-31 19:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_alter_shoe_closet_name_alter_shoe_color_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.CharField(max_length=100, unique=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
    ]
