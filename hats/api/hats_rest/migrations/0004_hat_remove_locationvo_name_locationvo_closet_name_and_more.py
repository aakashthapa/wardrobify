# Generated by Django 4.0.3 on 2024-01-31 15:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_rename_brand_name_hats_fabric_remove_hats_size_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fabric', models.CharField(max_length=50)),
                ('style_name', models.CharField(max_length=50)),
                ('color', models.CharField(max_length=50)),
                ('picture_url', models.URLField(blank=True, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='locationvo',
            name='name',
        ),
        migrations.AddField(
            model_name='locationvo',
            name='closet_name',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='locationvo',
            name='section_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='locationvo',
            name='shelf_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='locationvo',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.DeleteModel(
            name='Hats',
        ),
        migrations.AddField(
            model_name='hat',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='locations', to='hats_rest.locationvo'),
        ),
    ]
