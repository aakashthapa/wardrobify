from django.db import models
from django.urls import reverse
class LocationVO(models.Model):
   import_href = models.CharField(max_length=200, unique=True, null=True)
   name = models.CharField(max_length=200, null= True)
class Hats(models.Model):
   hat_name = models.CharField(max_length=200, null=True)
   fabric = models.CharField(max_length=200)
   style = models.CharField(max_length=200)
   color = models.CharField(max_length=200)
   picture_url = models.URLField(max_length=200, null=True)
   location = models.ForeignKey(
      LocationVO,
      related_name="hats",
      on_delete=models.CASCADE,
   )
   def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"pk": self.pk})
   